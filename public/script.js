// Déclaration des variables
let requestName = document.getElementById("requestName")
let requestService = document.getElementById("requestService")
let requestDeadLine = document.getElementById("requestDeadLine")
let isEmergency = document.getElementById("isEmergency")
let requestdescription = document.getElementById("requestDescription")
let startDate = document.getElementById("startDate")
let endDate = document.getElementById("endDate")
let inputData = document.getElementById("inputData")
let outputData = document.getElementById("outputData")

const submitButton = document.getElementById("submitButton")



// Vérification des données et désactivation de l'envoi
requestService.addEventListener("change", function () {
    const index = requestService.selectedIndex
    if (index == 0) {
        submitButton.disabled = true
    } else {
        submitButton.disabled = false
    }
})

requestName.addEventListener("change", () => {
    if (requestName.value == "") {
        submitButton.disabled = true
    } else {
        submitButton.disabled = false
    }
})

//Envoi des données

//let requestDataMail = JSON.parse(requestData)

submitButton.addEventListener("click", function (event) {
    event.preventDefault

    let requestData = {
        "Nom du demandeur": requestName.value,
        
        "Service du demandeaur": requestService.value,
        "Echéance": requestDeadLine.value,
        //"Urgent" : isEmergency,
        "Description de la requête": requestdescription.value,
        "Date de début": startDate.value,
        "Date de fin": endDate.value,
        "Données d'entrées": inputData.value,
        "Données de sortie": outputData.value
    }
    
    localStorage.setItem("Données de la requête", JSON.stringify(requestData))
    let requestDataMail = encodeURIComponent(JSON.stringify(requestData))
    window.open(`mailto:sgafsi@en3s.net?subject=Demande de requête&body=${requestDataMail}`);
    //window.print()
    //window.open("mailto:sgafsi@en3s.net?subject=Demande de requête&body="&requestData);
})